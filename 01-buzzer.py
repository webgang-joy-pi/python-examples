#!/usr/bin/python

import RPi.GPIO as GPIO         #importieren der benoetigten Bibliotheken
import time
# upd WebGang 03/07/2022
buzzer_pin = 12  # zoemer pin    #buzzer_pin wird definiert
pieplengte = 0.5 # suggestie: tussen 0.1 en 0.9

GPIO.setmode(GPIO.BOARD)
GPIO.setup(buzzer_pin, GPIO.OUT)

print('Zoemer - GPIO 12 - DIP alle OFF; lengte: ', pieplengte)

GPIO.output(buzzer_pin, GPIO.HIGH)      #Gebe Geraeusch aus
#time.sleep(pieplengte)  #warte eine halbe Sekunde
time.sleep(pieplengte)   # hou piep aan (pieplengte zie boven)

GPIO.output(buzzer_pin, GPIO.LOW)       #Stoppe Geraeuschausgabe

print('Zoemer: Eind')

GPIO.cleanup()
