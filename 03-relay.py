#!/usr/bin/python

import RPi.GPIO as GPIO
import time
# upd WebGang 03/07/2022
# definiere relay pin
relay_pin = 40
schakellengte = 2.5 # seconden geschakeld blijven, dan terug af

# Board Modus GPIO.BOARD
GPIO.setmode(GPIO.BOARD)
# relay_pin als Ausgang
GPIO.setup(relay_pin, GPIO.OUT)

print('Relais (schakelcontact) - GPIO 40 - DIP alle OFF; lengte (sec): ', schakellengte)

# Oeffne Relais
GPIO.output(relay_pin, GPIO.LOW)
# warte eine halbe Sekunde
# time.sleep(2.5)
time.sleep(schakellengte)
# schliesse Relais
GPIO.output(relay_pin, GPIO.HIGH)

print('Realais: Eind')
GPIO.cleanup()
