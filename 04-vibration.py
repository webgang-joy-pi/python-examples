#!/usr/bin/python

import RPi.GPIO as GPIO
import time
# upd WebGang 03/07/2022
# definieren des Vibrationspins
vibration_pin = 13
bibberlengte = 1
# setze Boardmodus zu GPIO.BOARD
GPIO.setmode(GPIO.BOARD)

# lege Vibrationspin als Ausgang fest
GPIO.setup(vibration_pin, GPIO.OUT)

print('Bibberen: DIP 2-1 aan, bibbermotor GPIO pin 13')

# schalte Vibration ein
GPIO.output(vibration_pin, GPIO.HIGH)
# warte eine Sekunde
# time.sleep(1)
time.sleep(bibberlengte)
# schalte Vibration aus
GPIO.output(vibration_pin, GPIO.LOW)
time.sleep(bibberlengte)
# tweede keer, gms stil effect bv met loopje
GPIO.output(vibration_pin, GPIO.HIGH)
time.sleep(bibberlengte)
GPIO.output(vibration_pin, GPIO.LOW)
# einde
print('Bibberen: Eind')
GPIO.cleanup()
