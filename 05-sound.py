#!/usr/bin/python

import RPi.GPIO as GPIO
import time

# upd WebGang 03/07/2022
sound_pin = 18
iTeller = 0
# GPIO mode wird auf GPIO.BOARD gesetzt
GPIO.setmode(GPIO.BOARD)
# sound_pin wird als Eingang festgelegt
GPIO.setup(sound_pin, GPIO.IN, pull_up_down=GPIO.PUD_UP)
print('Geluid ontdekken - STOP= CTRL-C - GPIO 18 - DIP alle OFF ; draai met klok=ongevoeliger')

try:
    while True:
        # ueberpruefe ob ein Geraeusch erkannt wird
        if(GPIO.input(sound_pin)==GPIO.LOW):
            print('Geluid ontdekt')
            time.sleep(0.1)
            iTeller += 1
            print('meting', iTeller)
except KeyboardInterrupt:
    # Strg+c beendet das Programm
    print('Geluid ontdekken: Eind')
    GPIO.cleanup()
