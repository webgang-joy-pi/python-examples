import RPi.GPIO as GPIO
import dht11
# upd WebGang 03/07/2022
# initialize GPIO
GPIO.setwarnings(False)
GPIO.setmode(GPIO.BCM)
GPIO.cleanup()

print('Temp. en vochtigheid: bord no 7, GPIO4 , DIP af')
# read data using pin 14 ## ??
instance = dht11.DHT11(pin = 4)
result = instance.read()

while not result.is_valid():  # read until valid values
    result = instance.read()

print("Temperatuur: %-3.1f C" % result.temperature)
print("Rel. luchtvochtigh: %-3.1f %%" % result.humidity)
