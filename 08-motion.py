#!/usr/bin/python
# -*- coding: utf-8 -*-

import RPi.GPIO as GPIO
import time             #Importieren der Bibliotheken
# upd WebGang 03/07/2022

motion_pin = 16  #Den Pin des Bewegungssensors einer Variable zuweisen.

GPIO.setmode(GPIO.BOARD) #Die GPIO Boardkonfiguration benutzen.
GPIO.setup(motion_pin, GPIO.IN)  #Der Pin der Deklarierten Variable wird als Input gesetzt.

print('Beweging detectie (PIR sensor): DIP af, GPIO pin 16, CTRL-C = stop')
time.sleep(3) # tijd om tekst te lezen
try:                        # Beginn einer Schleife
    while True:             
       if(GPIO.input(motion_pin) == 0): # Wenn der Sensor Input = 0 ist
             print ("Toestand rustig ...") # Wird der print Befehl ausgeführt
       elif(GPIO.input(motion_pin) == 1): # Wenn der Sensor Input = 1 ist
             print ("Beweging gezien!")  # Wird der print Befehl ausgeführt
       time.sleep(0.1) # 0,1 Sekunde Warten
except KeyboardInterrupt:
    GPIO.cleanup()     # Gibt GPIO Ports wieder frei.
