#!/usr/bin/python
# -*- coding: utf-8 -*-
# Author : www.modmypi.com
# Link: https://www.modmypi.com/blog/hc-sr04-ultrasonic-range-sensor-on-the-raspberry-pi
# upd WebGang 03/07/2022

import RPi.GPIO as GPIO
import time

GPIO.setmode(GPIO.BOARD) # Setze die GPIO Boardkonfiguration ein.

TRIG = 36    # Variablendeklaration 
ECHO = 32    # Variablendeklaration
speedOfSignalmps = 343
heenEnWeer = 2

print('Bereken afstand met geluid aan (m/s): ', speedOfSignalmps)

#print ("Entfernung wird ermittelt.") # Ausgabe von Text in der Konsole

GPIO.setup(TRIG,GPIO.OUT) # Variable TRIG als Output festlegen.
GPIO.setup(ECHO,GPIO.IN)  # Variable ECHO als Input festlegen.

GPIO.output(TRIG, False)
#print ("Warte auf den Sensor.")
print ("Meting uitvoeren, wacht...")
time.sleep(2) # 2 Sekunden Wartezeit.

GPIO.output(TRIG, True)  # Sendet ein Ultraschallsignal
time.sleep(0.00001)      # Wartet 0,00001 Sekunden
GPIO.output(TRIG, False) # Beendet das senden des Ultraschallsignals

while GPIO.input(ECHO)==0:
  pulse_start = time.time()

while GPIO.input(ECHO)==1:
  echo_back = time.time()

pulse_duration = echo_back - pulse_start # Berechnung für die Dauer Des Pulses

distance = pulse_duration * speedOfSignalmps  # Berechnung zur Bestimmung der Entfernung.

distance = round(distance/heenEnWeer, 2)      # Ergebnis wird auf 2 Nachkommastellen gerundet.

print ("Afstand:",distance,"cm")    # Konsolenausgabe der Distanz in cm.

GPIO.cleanup() # Gibt GPIO Ports wieder frei.
