# python-examples

For WebGang adapted versions of original python example code with Joy-Pi experimental suitcase.
Voor WebGang aangepaste versies van originele voorbeeldcode in python bij de Joy-Pi koffer.

## Name
python examples for use with Joy-Pi experiment suitcase also known as CrowPi in USA.

## Description
Just the same example files as provided, but with small adjustments, like dutch language comments and output and included settings info about DIP switches, variable declaration for parameters etc.
Files are numbered like in the documentation of JoyPi Manual under "7 Lesson(s)".

## Installation
All made in Thonny python IDE/editor on a Raspberry Pi 3B+ inside of the Joy-Pi suitcase, so should run on that and similar.

## Usage
Can be loaded in and run from the Thonny IDE.
Can be run from commandline with "sudo python3 ...py".

## Support
Mail WebGang

## Roadmap
Target is to edit all included lessons; start with 9 (of about 21)

## Authors and acknowledgment
- see code for original authors (al kept)

## License
- GPL or see original license

## Project status
- in preparation for use in broadcast in/late summer 2022.

## Getting started
- see original Joy-Pi code and documentation

